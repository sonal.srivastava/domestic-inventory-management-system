import org.junit.jupiter.api.Test;

import java.text.ParseException;
import java.util.Set;

public class InventoryTest {
    @Test
    void shouldGetDateFromString() throws ParseException {
        Inventory inventory = new Inventory();
        inventory.getDateFromString("2022-08-01");

    }

    @Test
    void shouldAddANewItemToInventory() throws ParseException {
        Inventory inventory = new Inventory();
        //Item item = new Item("soap", "2022-08-01");
        inventory.addItemToInventory("soap", "2022-08-01");

    }

    @Test
    void shouldBeAbleToReturnItemsThatExpireOnAGivenDate() throws ParseException {
        Inventory inventory = new Inventory();
        inventory.addItemToInventory("soap", "2022-08-01");
        inventory.addItemToInventory("kurkure", "2022-08-01");
        inventory.addItemToInventory("fenugrik", "2022-08-01");
        inventory.addItemToInventory("haldi", "2022-08-02");
        Set<Item> itemsThatExpireOnAGivenDate = inventory.getItemsThatExpireOnDate("2022-08-06");
        System.out.println("************************");
        for (Item item:itemsThatExpireOnAGivenDate) {
            item.displayItemDetails();
        }
    }

    @Test
    void shouldBeAbleToReturnItemsThatExpireInTheNext7Days() throws ParseException {
        Inventory inventory = new Inventory();
        inventory.addItemToInventory("soap", "2022-07-10");
        inventory.addItemToInventory("kurkure", "2022-07-7");
        inventory.addItemToInventory("fenugrik", "2022-07-10");
        inventory.addItemToInventory("haldi", "2022-08-02");
        Set<Item> itemsThatExpireInTheNext7Days = inventory.getItemsThatExpireInNext7Days();
        System.out.println("************************");
        for (Item item:itemsThatExpireInTheNext7Days) {
            item.displayItemDetails();
        }
    }

    @Test
    void shouldBeAbleToMarkORREMOVEAnItemConsumed() throws ParseException {
        Inventory inventory = new Inventory();
        inventory.addItemToInventory("soap", "2022-07-10");
        inventory.addItemToInventory("kurkure", "2022-07-7");
        inventory.addItemToInventory("fenugrik", "2022-07-10");
        inventory.addItemToInventory("haldi", "2022-08-02");
        inventory.markORREMOVEItemAsConsumed("haldi", "2022-08-02");
    }
    @Test
    void shouldBeAbleToMarkAnItemConsumed() throws ParseException {
        Inventory inventory = new Inventory();
        inventory.addItemToInventory("soap", "2022-08-02");
        inventory.addItemToInventory("kurkure", "2022-08-02");
        inventory.addItemToInventory("fenugrik", "2022-07-10");
        inventory.addItemToInventory("haldi", "2022-08-02");
        inventory.markItemAsConsumed("haldi", "2022-08-02");
    }
    @Test
    void shouldMarkExpiredItemsAsExpired() throws ParseException {
        Inventory inventory = new Inventory();
        inventory.addItemToInventory("soap", "2022-07-02");
        inventory.addItemToInventory("kurkure", "2022-07-05");
        inventory.addItemToInventory("fenugrik", "2022-07-10");
        inventory.addItemToInventory("haldi", "2022-08-02");
        inventory.markExpiredItemsAsExpired();
    }
}
