import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class Inventory {
    long millisecondInADay = 86400000;
    HashMap<Date, ExpiryDate> expiryDateObjectMap=new HashMap<>();
    SimpleDateFormat SDF = new SimpleDateFormat("yyyy-MM-dd");
    public void addItemToInventory(String item, String expiryDateString) throws ParseException {
        ExpiryDate expiryDateObject;
        Date expiryDate = getDateFromString(expiryDateString);
        if (!expiryDateObjectMap.containsKey(expiryDate)){

            expiryDateObject = new ExpiryDate(expiryDate);
            expiryDateObjectMap.put(expiryDate, expiryDateObject);
        }
        else{
            expiryDateObject = expiryDateObjectMap.get(expiryDate);
        }
        expiryDateObject.addItem(item);
    }

    public Date getDateFromString(String dateString) throws ParseException {

        return SDF.parse(dateString);

    }

    public Set<Item> getItemsThatExpireOnDate(String expiryDateString) throws ParseException {
        ExpiryDate expiryDateObject;
        Date expiryDate = getDateFromString(expiryDateString);
        System.out.println("Searching for date in expiryDateObjectMap ::: "+expiryDate);
        if (!expiryDateObjectMap.containsKey(expiryDate)){
            System.out.println("$$$$ Date NOT There $$$$");
            return new HashSet<>();
        }
        expiryDateObject = expiryDateObjectMap.get(expiryDate);
        return expiryDateObject.getItems();
    }

    public Date getTodayDate() throws ParseException {
        Calendar rightNow = Calendar.getInstance();
        Date dateToday = SDF.parse(SDF.format(rightNow.getTime()));
        System.out.println(dateToday);
        return dateToday;
    }

    public HashSet<Item> getItemsThatExpireInNext7Days() throws ParseException {
        Date dateToday=new Date();
        long epochTime = dateToday.getTime();
        HashSet<Item> itemsThatExpireInNext7Days= new HashSet<>();
        for( int day = 0 ; day<7 ; day++){
            long epochTimeNextDay=epochTime+ millisecondInADay *day;
            Calendar calDay = Calendar.getInstance();
            calDay.setTimeInMillis(epochTimeNextDay);
            String dateNextString = SDF.format(calDay.getTime());
            //System.out.println("PRINCEEE ===>  "+dateNextString);
//            if (expiryDateObjectMap.containsKey(dateNextString)){
//                ExpiryDate dateNextObject = expiryDateObjectMap.get(dateNextString);
//                itemsThatExpireInNext7Days.retainAll(dateNextObject.getItems());
//            }
            itemsThatExpireInNext7Days.addAll(getItemsThatExpireOnDate(dateNextString));
            System.out.println(itemsThatExpireInNext7Days.size());
        }
        return itemsThatExpireInNext7Days;
    }

    public void markORREMOVEItemAsConsumed(String itemString, String dateString) throws ParseException {
        ExpiryDate expiryDateObject;
        Date expiryDate = getDateFromString(dateString);
        if (!expiryDateObjectMap.containsKey(expiryDate)){

            expiryDateObject = new ExpiryDate(expiryDate);
            expiryDateObjectMap.put(expiryDate, expiryDateObject);
        }
        else{
            expiryDateObject = expiryDateObjectMap.get(expiryDate);
        }
        expiryDateObject.markORREMOVEItemAsConsumedWithExpiry(itemString);
    }

    public void markItemAsConsumed(String itemString, String dateString) throws ParseException {
        ExpiryDate expiryDateObject;
        Date expiryDate = getDateFromString(dateString);
        if (!expiryDateObjectMap.containsKey(expiryDate)){

            expiryDateObject = new ExpiryDate(expiryDate);
            expiryDateObjectMap.put(expiryDate, expiryDateObject);
        }
        else{
            expiryDateObject = expiryDateObjectMap.get(expiryDate);
        }
        expiryDateObject.markItemAsConsumedWithExpiry(itemString);
    }

    public void markExpiredItemsAsExpired() throws ParseException {
        for (Date date:
             expiryDateObjectMap.keySet()) {
            if (date.compareTo(getTodayDate())<0){
                ExpiryDate expiryDateObject = expiryDateObjectMap.get(date);
                expiryDateObject.markExpiredItemsAsExpiredWithThisExpiry();
            }
        }
    }
}
