import java.util.Date;
import java.util.HashMap;
import java.util.Set;

public class ExpiryDate {
    Date date;
    HashMap<String, Item> itemObjectMap=new HashMap<>();
    HashMap<Item, Integer> itemCountHashMap=new HashMap<>();

    public ExpiryDate(Date expiryDate) {
        this.date=expiryDate;
    }

    public void addItem(String item) {
        Item itemObject;
        int count=0;
        if (!itemObjectMap.containsKey(item)){
            itemObject = new Item(item.toLowerCase(), date.toString());
            itemObjectMap.put(item, itemObject);
        }
        else{
            itemObject = itemObjectMap.get(item);
            count = itemCountHashMap.get(itemObject);
        }
        itemCountHashMap.put(itemObject, count+1);
        System.out.println("added this item :::: "+item+"for date ::: "+date);
    }

    public Set<Item> getItems() {
        return itemCountHashMap.keySet();
    }

    public void markORREMOVEItemAsConsumedWithExpiry(String itemString) {
        Item itemObject = null;
        int count=0;
        if (itemObjectMap.containsKey(itemString)){
            itemObject = itemObjectMap.get(itemString);
            count = itemCountHashMap.get(itemObject);
        }
        if (count==1){
            itemObjectMap.remove(itemString);
            itemCountHashMap.remove(itemObject);
        }
        itemCountHashMap.put(itemObject, count-1);
    }

    public void markItemAsConsumedWithExpiry(String itemString) {
        System.out.println(date);
        System.out.println("*******"+itemString);
        for (String s:itemObjectMap.keySet()) {
            System.out.println(s);
        }
        Item itemObject = itemObjectMap.get(itemString);
        itemObject.displayItemDetails();
        //itemCountHashMap.put(itemObject, count-1);
        itemObject.updateItemState(ItemStateEnum.CONSUMED);
        System.out.println("*************");
        itemObject.displayItemDetails();
    }

    public void markExpiredItemsAsExpiredWithThisExpiry() {
        for (Item itemObject:
             itemCountHashMap.keySet()) {
            itemObject.displayItemDetails();
            itemObject.updateItemState(ItemStateEnum.EXPIRED);
            System.out.println("*************");
            itemObject.displayItemDetails();
        }
    }
}
