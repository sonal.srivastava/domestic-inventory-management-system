public class Item {
    private final String itemName;
    private final String itemExpiry;
    private final String itemCategory;

    ItemStateEnum itemState;
    public Item(String itemName, String expiryDateString) {
        this.itemName=itemName;
        this.itemCategory="NA";
        this.itemExpiry=expiryDateString;
        this.itemState=ItemStateEnum.IN_STOCK;
    }

    public void displayItemDetails() {
        System.out.println(itemName+" ("+itemCategory+")  "+itemExpiry+" ### "+itemState);
    }

    public void updateItemState(ItemStateEnum consumed) {
        itemState=consumed;
    }
}
