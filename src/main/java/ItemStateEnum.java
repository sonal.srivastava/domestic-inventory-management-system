public enum ItemStateEnum {
    IN_STOCK,
    CONSUMED,
    EXPIRED
}
